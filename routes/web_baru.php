<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function () use ($router) {
	#meja
    $router->get('master/meja', 'Master\MejaController@get');
    $router->post('master/meja', 'Master\MejaController@save');
    $router->delete('master/meja', 'Master\MejaController@delete');
    #Kategori
    $router->get('master/kategori', 'Master\KategoriController@get');
    $router->post('master/kategori', 'Master\KategoriController@save');
    $router->delete('master/kategori', 'Master\KategoriController@delete');
    #Menu
    $router->get('master/menu', 'Master\MenuController@get');
    $router->post('master/menu', 'Master\MenuController@save');
    $router->delete('master/menu', 'Master\MenuController@delete');

    #FoodCost
    $router->get('master/food_cost', 'Master\FoodCostController@get');
    $router->post('master/food_cost', 'Master\FoodCostController@save');
    $router->delete('master/food_cost', 'Master\FoodCostController@delete');

    #Paid
    $router->get('master/paid', 'Master\PaidController@get');
    $router->post('master/paid', 'Master\PaidController@save');
    $router->delete('master/paid', 'Master\PaidController@delete');
    


    $router->post('transaksi/penjualan', 'Transaksi\PenjualanController@save');
    $router->post('transaksi/penjualan_detail', 'Transaksi\PenjualanController@saveDetail');

    $router->post('transaksi/paid', 'Transaksi\PaidController@save');
    $router->post('transaksi/rekap_kas', 'Transaksi\RekapKasController@save');
});