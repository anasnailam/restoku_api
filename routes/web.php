<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    $router->get('master/meja', 'Master\MejaController@get');
    $router->post('master/meja', 'Master\MejaController@save');
    $router->delete('master/meja', 'Master\MejaController@delete');

    $router->post('transaksi/penjualan', 'Transaksi\PenjualanController@save');
    $router->post('transaksi/penjualan_detail', 'Transaksi\PenjualanController@saveDetail');

    $router->post('transaksi/paid', 'Transaksi\PaidController@save');
    $router->post('transaksi/rekap_kas', 'Transaksi\RekapKasController@save');
});