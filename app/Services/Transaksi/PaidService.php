<?php

namespace App\Services\Transaksi;

use Illuminate\Support\Facades\DB;

class PaidService
{

    public function save($id_toko, $param)
    {
        $guids = [];
        $data_insert = [];

        foreach ($param as $k => $v) {
            array_push($guids, $v['guid']);

            $v['id_toko'] = $id_toko;
            $v['last_sync'] = date('Y-m-d H:i:s');
            $data_insert[$v['guid']] = $v;
        }

        $data = DB::table('transaksi_paid')
            ->whereIn('guid', $guids)
            ->get();

        $exists_guid = [];
        foreach ($data as $k => $v) {
            if (!empty($v)) {
                array_push($exists_guid, $v->guid);
            }
        }

        // UPDATE EXISTING DATA
        $bulk_update = [];
        foreach ($exists_guid as $k => $v) {
            unset($data_insert[$v]['id']);

            DB::table('transaksi_paid')
                ->where('guid', $v)
                ->update($data_insert[$v]);

            array_push($bulk_update, $data_insert[$v]);
        }

        // guid yang belom ada di db
        $result = array_diff($guids, $exists_guid);

        $bulk_data_insert = [];
        foreach ($result as $k => $v) {
            array_push($bulk_data_insert, $data_insert[$v]);
        }

        if (count($bulk_data_insert) > 0) {
            DB::table('transaksi_paid')->insert($bulk_data_insert);
        }

        foreach ($bulk_update as $k => $v) {
            array_push($bulk_data_insert, $v);
        }

        return $bulk_data_insert;

    }

}