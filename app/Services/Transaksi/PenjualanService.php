<?php

namespace App\Services\Transaksi;

use Illuminate\Support\Facades\DB;

class PenjualanService
{

    public function save($id_toko, $param)
    {
        $guids = [];
        $data_insert = [];

        foreach ($param as $k => $v) {
            array_push($guids, $v['guid']);

            unset($v['id']);
            unset($v['id_sync']);

            $v['id_toko'] = $id_toko;
            $v['last_sync'] = date('Y-m-d H:i:s');
            $data_insert[$v['guid']] = $v;
        }

        $data = DB::table('transaksi_penjualan')
            ->whereIn('guid', $guids)
            ->get();

        $exists_guid = [];
        foreach ($data as $k => $v) {
            if (!empty($v)) {
                array_push($exists_guid, $v->guid);
            }
        }

        // guid yang belom ada di db
        $result = array_diff($guids, $exists_guid);

        $bulk_data_insert = [];
        foreach ($result as $k => $v) {
            array_push($bulk_data_insert, $data_insert[$v]);
        }

        DB::table('transaksi_penjualan')->insert($bulk_data_insert);

        return $bulk_data_insert;
    }

    public function saveDetail($id_toko, $param)
    {
        $guids = [];
        $data_insert = [];

        foreach ($param as $k => $v) {
            array_push($guids, $v['guid']);

            unset($v['id']);
            unset($v['id_sync']);
            unset($v['id_penjualan_sync']);
            unset($v['nama_menu']);
            unset($v['catatan']);

            $v['id_toko'] = $id_toko;
            $v['last_sync'] = date('Y-m-d H:i:s');
            $data_insert[$v['guid']] = $v;
        }

        $data = DB::table('transaksi_penjualan_detail')
            ->whereIn('guid', $guids)
            ->get();

        $exists_guid = [];
        foreach ($data as $k => $v) {
            if (!empty($v)) {
                array_push($exists_guid, $v->guid);
            }
        }

        // guid yang belom ada di db
        $result = array_diff($guids, $exists_guid);

        $bulk_data_insert = [];
        foreach ($result as $k => $v) {
            $penjualan = DB::table('transaksi_penjualan')
                ->select('id')
                ->where('guid', $data_insert[$v]['guid_penjualan'])
                ->first();
            $data_insert[$v]['id_penjualan'] = $penjualan->id;
            array_push($bulk_data_insert, $data_insert[$v]);
        }

        DB::table('transaksi_penjualan_detail')->insert($bulk_data_insert);


        return $bulk_data_insert;
    }
}