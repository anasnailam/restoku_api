<?php

namespace App\Services\Transaksi;

use Illuminate\Support\Facades\DB;

class RekapKasService
{

    public function save($id_toko, $param)
    {
        $guids = [];
        $data_insert = [];

        foreach ($param as $k => $v) {
            array_push($guids, $v['guid']);

            unset($v['guid_penjualan_terakhir']);

            $v['id_toko'] = $id_toko;
            $v['id_penjualan_terakhir'] = $this->getPenjualanTerakhir($id_toko);
            $v['last_sync'] = date('Y-m-d H:i:s');
            $data_insert[$v['guid']] = $v;
        }

        $data = DB::table('transaksi_rekap_kas')
            ->whereIn('guid', $guids)
            ->get();

        $exists_guid = [];
        foreach ($data as $k => $v) {
            if (!empty($v)) {
                array_push($exists_guid, $v->guid);
            }
        }

        // guid yang belom ada di db
        $result = array_diff($guids, $exists_guid);

        $bulk_data_insert = [];
        foreach ($result as $k => $v) {
            array_push($bulk_data_insert, $data_insert[$v]);
        }

        if (count($bulk_data_insert) > 0) {
            DB::table('transaksi_rekap_kas')->insert($bulk_data_insert);
        }

        return $bulk_data_insert;

    }

    function getPenjualanTerakhir($id_toko)
    {
        $data = DB::table('transaksi_penjualan')
            ->select('id')
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->where('id_toko', $id_toko)
            ->first();

        return $data->id;
    }
}