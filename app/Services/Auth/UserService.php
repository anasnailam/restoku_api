<?php

namespace App\Services\Master;

use Illuminate\Support\Facades\DB;

class UserService
{

    public function login($data)
    {
        return DB::table('master_meja')
            ->where('username', '=', $data['username'])
            ->where('password', '=', $data['password'])
            ->whereNull('deleted_at')
            ->get();
    }

}