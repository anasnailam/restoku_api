<?php

namespace App\Services\Master;

use Illuminate\Support\Facades\DB;

class KategoriService
{

    public function get($id_toko)
    {
        $kategori = DB::table('master_kategori')
            ->where('id_toko', '=', $id_toko)
            ->whereNull('deleted_at')
            ->get();

        foreach ($kategori as $k => $v) {
            $update_field = [];
            if (empty($v->guid)) {
                $guid = $this->appGuid();
                $update_field['guid'] = $guid;

                $v->guid = $guid;
            }
            if (empty($v->last_sync_at)) {
                $last_sync = date('Y-m-d H:i:s');
                $update_field['last_sync_at'] = $last_sync;

                $v->last_sync_at = $last_sync;
            }

            if (count($update_field) > 0) {
                DB::table('master_kategori')
                    ->where('id', $v->id)
                    ->update($update_field);
            }

        }

        return $kategori;
    }

    public function save($data)
    {
        $result = [];
        foreach ($data['meja'] as $k => $v) {
            $count_meja = DB::table('master_kategori')->where('guid', '=', $v['guid'])->count();

            if ($count_meja <= 0) {
                DB::table('master_kategori')->insert([
                    'guid' => $v['guid'],
                    'nama' => $v['nama'],
                    'tipe' => $v['tipe'],
                    'is_bahan_baku' => $v['is_bahan_baku'],
                    'id_toko' => $data['id_toko'],
                    'meja' => $v['nomor'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'last_sync_at' => date('Y-m-d H:i:s')
                ]);
            } else {
                DB::table('master_kategori')
                    ->where('guid', '=', $v['guid'])
                    ->update([
                        'meja' => $v['nomor'],
                        'updated_at' => date('Y-m-d H:i:s'),
                        'last_sync_at' => date('Y-m-d H:i:s')
                    ]);
            }

            $result += [
                'guid' => $v['guid'],
                'last_sync_at' => $v['last_sync_at'],
            ];
        }

        return $result;
    }

    public function delete($id)
    {
        return DB::table('master_kategori')
            ->where('id', '=', $id)
            ->update([
                'deleted_at' => date('Y-m-d H:i:s')
            ]);
    }

    function appGuid()
    {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

}