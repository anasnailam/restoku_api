<?php

namespace App\Http\Controllers\Transaksi;

use App\Http\Controllers\Controller;
use App\Services\Transaksi\PenjualanService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PenjualanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function save(Request $request, PenjualanService $service)
    {
        $id_toko = $request->all()['id_toko'];
        $input = $request->json()->all();
//        Log::info(json_encode($input));

        $response = $service->save($id_toko, $input);

        return response()->json($response);
    }

    public function saveDetail(Request $request, PenjualanService $service)
    {
        $id_toko = $request->all()['id_toko'];
        $input = $request->json()->all();
//        Log::info(json_encode($input));

        $response = $service->saveDetail($id_toko, $input);

        return response()->json($response);
    }

}
