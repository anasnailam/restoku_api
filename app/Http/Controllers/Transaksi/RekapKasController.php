<?php

namespace App\Http\Controllers\Transaksi;

use App\Http\Controllers\Controller;
use App\Services\Transaksi\RekapKasService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RekapKasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function save(Request $request, RekapKasService $service)
    {
        $id_toko = $request->all()['id_toko'];
        $input = $request->json()->all();

        $response = $service->save($id_toko, $input);

        return response()->json($response);
    }


}
