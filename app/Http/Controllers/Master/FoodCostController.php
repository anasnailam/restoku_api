<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Services\Master\FoodCostService;
use Illuminate\Http\Request;

class FoodCostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get(Request $request, FoodCostService $service)
    {
        $input = $request->all();

        $data = $service->get($input['id_toko']);
        return response()->json($data);
    }

    public function save(Request $request, FoodCostService $service)
    {
        $input = $request->all();

        $data = $service->save($input);

        return response()->json($data);
    }

}
