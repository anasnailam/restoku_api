<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Services\Master\KategoriService;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get(Request $request, KategoriService $service)
    {
        $input = $request->all();

        $data = $service->get($input['id_toko']);
        return response()->json($data);
    }

    public function save(Request $request, KategoriService $service)
    {
        $input = $request->all();

        $data = $service->save($input);

        return response()->json($data);
    }

}
