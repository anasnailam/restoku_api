<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Services\Master\MenuService;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get(Request $request, MenuService $service)
    {
        $input = $request->all();

        $data = $service->get($input['id_toko']);
        return response()->json($data);
    }

    public function save(Request $request, MenuService $service)
    {
        $input = $request->all();

        $data = $service->save($input);

        return response()->json($data);
    }

}
